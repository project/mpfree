<?php // $Id$ ?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?>">
  <?php if (!empty($block->subject)) : ?>
  <h3 class="block-title"><span class="border"><?php print $block->subject ?></span></h3>
  <?php endif; ?>
  <div class="contents">
    <div class="space">
      <?php print $block->content ?>
    </div>
  </div>
</div>