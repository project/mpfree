<?php // $Id$ ?>
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status .' '. $zebra; ?>">
  <h3><?php print $title ?><?php if ($comment->new) : ?><span class="new">&nbsp;/&nbsp;<?php print drupal_ucfirst($new); ?></span><?php endif; ?></h3>
  <div class="content">
    <?php print $picture; ?>
    <?php print $content; ?>
    <?php print $signature; ?>
  </div>
  <?php if ($submitted || $links || $taxonomy) : ?>
  <div class="details">
    <?php if ($submitted) : ?><div class="submitted"><span class="icon"></span>&nbsp;&nbsp;<?php print $submitted; ?></div><?php endif; ?>
    <?php if ($links) : ?><div class="links"><span class="icon"></span><?php print $links; ?></div><?php endif; ?>
  </div>
  <?php endif; ?>
</div>