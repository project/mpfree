<?php // $Id$ ?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) : print ' sticky'; endif; ?><?php if (!$status) : print ' node-unpublished'; endif; ?>">
<?php if ($page == 0) : ?><!-- IF LISTING PAGES -->
  <h1 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h1>
  <div style="clear:both"></div>
  <?php if ($submitted || $links || $terms) : ?>
  <div class="details-fp">
    <div class="space">
      <?php if ($submitted) : print $submitted .' | '; endif; if ($links) : print $links .' | '; endif; if ($terms) : print $terms; endif; ?>
    </div>
  </div>
  <?php endif; ?>
<?php endif; ?><!-- IF LISTING PAGES -->
  <div style="clear:both"></div>
  <div class="content">
    <?php print $picture; ?>
    <?php print $content; ?>
  </div>
  <div style="clear:both"></div>
  <?php if (($page != 0) && ($submitted || $links || $terms)) : ?>
  <div class="details">
    <?php if ($submitted) : ?><div class="submitted"><span class="icon"></span>&nbsp;&nbsp;<?php print $submitted; ?></div><?php endif; ?>
    <?php if ($links) : ?><div class="links"><span class="icon"></span><?php print $links; ?></div><?php endif; ?>
    <?php if ($terms) : ?><div class="terms"><span class="icon"></span><?php print $terms; ?></div><?php endif; ?>
  </div>
  <?php endif; ?>
</div>