<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if IE]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print base_path().path_to_theme(); ?>/css/ie-fix.css" type="text/css" />
    <![endif]-->
    <?php if($language->dir == 'rtl') { ?>
    <!--[if IE]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print base_path().path_to_theme(); ?>/css/ie-fix-rtl.css" type="text/css" />
    <![endif]-->
    <?php } ?>
  </head>
  <body>
    <?php if ($message) : ?>
      <div id="message">
        <?php print $message; ?>
        <div style="clear:both"></div>
      </div>
    <?php endif; ?>
    <div id="header">
      <div class="border">
        <div class="center">
          <?php if($logo) : ?>
            <a class="logo" href="<?php print $front_page ?>"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" /></a>
          <?php endif; ?>
          <?php if ($header) : ?>
          <div class="region<?php if (!$logo) { print ' wide'; } ?>">
            <?php print $header; ?>
          </div>
          <?php endif; ?>
        </div><!-- Center -->
      </div><!-- Border -->
    </div><!-- Header -->
    <?php if ($intro || $intro_image || $intro_description) : ?>
    <div id="intro">
      <div class="center">
        <div class="space">
          <?php print $intro; ?>
          <div style="clear:both"></div>
          <?php if ($intro_image) : ?>
            <div class="image">
              <?php print $intro_image; ?>
            </div>
          <?php endif;?>
          <?php if ($intro_description) : ?>
            <div class="description">
              <?php print $intro_description; ?>
            </div>
          <?php endif; ?>
        </div><!-- Space -->
      </div><!-- Center -->
      <div style="clear:both"></div><!-- Do not touch -->
    </div><!-- Intro -->
    <?php endif; ?>
    <?php if($top_a || $top_b || $top_c || $top_d || $top_e) : ?>
    <div style="clear:both"></div><!-- Do not touch -->
    <div id="top" class="in<?php print (bool) $top_a + (bool) $top_b + (bool) $top_c + (bool) $top_d + (bool) $top_e; ?>">
      <div class="border">
        <div class="center">
          <?php if($top_a) : ?>
          <div class="column A">
            <?php print $top_a; ?>
          </div>
          <?php endif; ?>
          <?php if($top_b) : ?>
          <div class="column B">
            <?php print $top_b; ?>
          </div>
          <?php endif; ?>
          <?php if($top_c) : ?>
          <div class="column C">
            <?php print $top_c; ?>
          </div>
          <?php endif; ?>
          <?php if($top_d) : ?>
          <div class="column D">
            <?php print $top_d; ?>
          </div>
          <?php endif; ?>
          <?php if($top_e) : ?>
          <div class="column E">
            <?php print $top_e; ?>
          </div>
          <?php endif; ?>
        </div><!-- Center -->
      <div style="clear:both"></div><!-- Do not touch -->
      </div><!-- Border -->
    </div><!-- Top -->
    <?php endif; ?>
    <div style="clear:both"></div><!-- Do not touch -->
    <div id="content">
      <div class="center">
        <?php if ($breadcrumb) : ?>
            <div id="breadcrumb">
              <div class="space">
                <?php print $breadcrumb; ?>
              </div>
            </div>
        <?php endif; ?>
        <div class="space">
          <?php if ($sidebar_left_top || $sidebar_left_bottom || ($mainmenu && $language->dir != 'rtl')) : ?>
          <div id="sidebar-left">
          <?php if ($sidebar_left_top) : ?>
            <div id="sidebar-left-top">
              <div class="space">
                <?php print $sidebar_left_top; ?>
                <div style="clear:both"></div>
              </div>
            </div>
            <div style="clear:both"></div><!-- Do not touch -->
          <?php endif; ?>
          <?php if ($mainmenu && $language->dir != 'rtl') : ?>
            <div id="mainmenu">
              <div class="space">
                <div class="bg"><div class="bg"><div class="bg">
                  <div class="space">
                    <?php print $mainmenu; ?>
                  </div>
                <div style="clear:both"></div>
                </div></div></div>
              </div><!-- Space -->
            </div><!-- Mainmenu -->
            <div style="clear:both"></div>
          <?php endif; ?>
          <?php if ($sidebar_left_bottom) : ?>
            <div id="sidebar-left-bottom">
              <div class="space">
                <?php print $sidebar_left_bottom; ?>
                <div style="clear:both"></div>
              </div>
            </div>
          <?php endif; ?>
          </div><!-- Sidebar-Left -->
          <?php endif; ?>
          <div id="main" class="<?php  if ((($language->dir != 'rtl') && ($sidebar_right && ($sidebar_left_top || $mainmenu || $sidebar_left_bottom))) || (($language->dir == 'rtl') && (($sidebar_right || $mainmenu) && ($sidebar_left_top || $sidebar_left_bottom)))) :  print 'narrow'; else : if ($sidebar_right || $sidebar_left_top || $mainmenu || $sidebar_left_bottom) : print 'normal'; endif; endif; ?><?php if($is_front) : print ' frontpage'; endif; ?>">
            <?php if($content_above) : ?>
            <div id="content_above">
              <?php print $content_above; ?>
            </div>
            <?php endif; ?>
            <div style="clear:both"></div>
            <div class="space">
              <?php if ($mission) : ?><div id="mission"><div class="border"><?php print $mission ?></div></div><?php endif; ?>
              <?php if ($title) : ?><h1 class="title"><?php print $title ?></h1><?php endif; ?>
              <?php if ($tabs) : ?><div id="tabs"><?php endif; ?>
              <?php if ($tabs) : ?><ul class="tabs primary"><?php print $tabs; ?></ul><?php endif; ?>
              <?php if ($tabs2) : ?><ul class="tabs secondary"><?php print $tabs2; ?></ul><?php endif; ?>
              <?php if ($tabs) : ?></div><?php endif; ?>
              <?php if ($show_messages && $messages) : print $messages; endif; ?>
              <?php print $help; ?>
              <?php print $content; ?>
            </div>
            <?php if($content_below) : ?>
            <div id="content_below">
              <?php print $content_below; ?>
            </div>
            <?php endif; ?>
          </div><!-- Main -->
          <?php if($sidebar_right || ($mainmenu && $language->dir == 'rtl')) : ?>
          <div id="sidebar-right">
            <?php if ($mainmenu && $language->dir == 'rtl') : ?>
            <div id="mainmenu">
              <div class="space">
                <div class="bg"><div class="bg"><div class="bg">
                  <div class="space">
                    <?php print $mainmenu; ?>
                  </div>
                <div style="clear:both"></div>
                </div></div></div>
              </div><!-- Space -->
            </div><!-- Mainmenu -->
            <div style="clear:both"></div>
            <?php endif; ?>
            <div class="space">
              <?php print $sidebar_right; ?>
              <div style="clear:both"></div>
            </div>
          </div><!-- Sidebar-Right -->
          <?php endif; ?>
        </div><!-- Space -->
      </div><!-- Center -->
    </div><!-- Content -->
    <?php if($bottom_left || $bottom_right) : ?>
    <div style="clear:both"></div>
    <div id="middle">
      <div class="center">
      <?php if ($bottom_left) : ?>
      <div id="left" <?php if (!$bottom_right) : print 'class="wide"'; endif; ?>>
        <?php print $bottom_left; ?>
      </div>
      <?php endif; ?>
      <?php if ($bottom_right) : ?>
      <div id="right" <?php if (!$bottom_left) : print 'class="wide"'; endif; ?>>
        <?php print $bottom_right; ?>
      </div>
      <?php endif; ?>
      <div style="clear:both"></div><!-- Do not touch -->
      </div><!-- Center -->
    </div><!-- Middle -->
    <?php endif; ?>
    <?php if($bottom_a || $bottom_b || $bottom_c || $bottom_d || $bottom_e) : ?>
    <div style="clear:both"></div><!-- Do not touch -->
    <div id="bottom" class="in<?php print (bool) $bottom_a + (bool) $bottom_b + (bool) $bottom_c + (bool) $bottom_d + (bool) $bottom_e; ?>">
      <div class="center">
        <div class="space">
          <?php if($bottom_a) : ?>
          <div class="column A">
            <?php print $bottom_a; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_b) : ?>
          <div class="column B">
            <?php print $bottom_b; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_c) : ?>
          <div class="column C">
            <?php print $bottom_c; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_d) : ?>
          <div class="column D">
            <?php print $bottom_d; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_e) : ?>
          <div class="column E">
            <?php print $bottom_e; ?>
          </div>
          <?php endif; ?>
        </div><!-- Space -->
      </div><!-- Center -->
      <div style="clear:both"></div><!-- Do not touch -->
    </div><!-- Bottom -->
    <?php endif; ?>
    <?php if($footer || $footer_message) : ?>
    <div style="clear:both"></div><!-- Do not touch -->
    <div id="footer">
      <div class="center">
        <div class="space">
          <?php print $footer; ?>
          <div style="clear:both"></div>
          <?php print $footer_message; ?>
        </div><!-- Space -->
      </div><!-- Center -->
    </div><!-- Footer -->
    <?php endif; ?>
    <?php print $closure ?>
  </body>
</html>