<?php // $Id$
function phptemplate_settings($saved_settings) {
  $defaults = array('colour' => 'blue');
  $settings = array_merge($defaults, $saved_settings);
  $form['colour'] = array(
    '#type' => 'select',
    '#title' => t('Colour scheme'),
    '#default_value' => $settings['colour'],
    '#options' => array(
      'blue' => t('Blue'),
      'light-blue' => t('Light blue'),
    )
  );
return $form;
}